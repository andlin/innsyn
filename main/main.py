# -*- coding: utf-8 -*-

from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4

class Innsyn():
    
    def __init__(self,  fields={}):
        self.fields = fields

    # Field positions
    pos = {
        # Om virksomheten
        "virksomhetens_navn": (70,  690,),

        # Krav om innsyn
        "krav_generell_info": (86,  600),
        "krav_innsyn": (86,  575),
        "krav_innsyn_meg_selv": (122,  555),
        "krav_innsyn_barn": (122,  537),
        "innsyn_alle_opplysninger": (122,  472),
        "innsyn_spesifisert": (122,  453),
        "info_sikkerhetstiltak": (86,  364),

        # Navn og kontaktsopplysninger
        "navn": (90,  315),
        "adresse1": (90,  285),
       "postnummer": (90,  260),
       "poststed": (200,  260),
       "tilleggsopplysninger": (90,  190),

       # Signatur
       "sted_dato": (90,  80),
       "sign": (300,  80),
    }

    def fill(self,  can,  field,  contens = "X"):
        "Fill a field. Looks up field position and fills with text from contents."
        x,  y = self.pos[field]
        can.drawString(x,  y, contens)

    def openExistingPdf(self):
        "Read PDF form"
        return PdfFileReader(open("innsynsmal.pdf", "rb"))

    def buildOverlayPdf(self):
        "Create an overlay PDF (field contents) that will be merged with form PDF."
        packet = io.BytesIO()
        self.fillOverlay(packet)
        # move to the beginning of the StringIO buffer
        packet.seek(0)
        return PdfFileReader(packet)

    def fillOverlay(self,  p):
        "Create overlay with the correct input elements checked/filled"
        can = canvas.Canvas(p, pagesize=A4)

        for k, v in self.fields.items():
            self.fill(can, k,  v)
        can.save()

    def main(self):
        new_pdf = self.buildOverlayPdf()
        existing_pdf = self.openExistingPdf()

        output = PdfFileWriter()
        page = existing_pdf.getPage(0)
        page.mergePage(new_pdf.getPage(0))
        output.addPage(page)
        # finally, write "output" to a real file
        outputStream = open("destination.pdf", "wb")
        output.write(outputStream)
        outputStream.close()    


i = Innsyn(
{
"virksomhetens_navn": "Another company",
"krav_generell_info": "X",
"krav_innsyn": "X", 
"krav_innsyn_meg_selv": "X", 
"krav_innsyn_barn": "X", 
"innsyn_alle_opplysninger": "X", 
"innsyn_spesifisert": "X", 
"info_sikkerhetstiltak": "X", 
"navn":  "Harald Bastesen", 
"adresse1":  "Gateveien 11", 
"postnummer": "1405", 
"poststed": "Bærum", 
"tilleggsopplysninger": "Kundenummer er 123451", 
"sted_dato": "Bærum, 18/5", 
"sign": "Harald Bastesen (sign)", 
})
i.main()
